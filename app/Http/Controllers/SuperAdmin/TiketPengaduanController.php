<?php

namespace App\Http\Controllers\SuperAdmin;

use Carbon\Carbon;
use App\Models\Tiket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LampiranTiket;
use App\Models\RiwayatTiket;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class TiketPengaduanController extends Controller
{
    public function lihatTiket()
    {
        // return 'OK';
        // Mengambil semua data dari tabel Tiket
        $data = Tiket::select(['id', 'judul_tiket', 'waktu_tiket','kategori_laporan','status_tiket','tingkat_urgensi'])->get(); // Pastikan 'status' di sini
        foreach ($data as $tiket) {
            $tiket->waktu_tiket = Carbon::parse($tiket->waktu_tiket)->format('Y-m-d');
        }
        return Datatables::of($data)->make(true);
    }

        public function update(Request $request)
        {
            try {
    
                // Validasi input jika diperlukan
                $request->validate([
                    'judul_tiket' => 'required',
                    'waktu_tiket' => 'required|date_format:Y-m-d',
                    'kategori_laporan' => 'required|in:Teknis,Akademis,Administratif,Lainnya',
                    'status_tiket' => 'required|in:Belum Diproses,Sedang Diproses,Selesai',
                    'tingkat_urgensi' => 'required|in:Rendah,Sedang,Tinggi',
                ], [
                    'judul_tiket.required' => 'Judul tiket wajib diisi.',
                    'waktu_tiket.required' => 'Waktu tiket wajib diisi.',
                    'waktu_tiket.date_format' => 'Format waktu tiket tidak valid. Gunakan format Y-m-d.',
                    'kategori_laporan.required' => 'Kategori laporan wajib diisi.',
                    'kategori_laporan.in' => 'Kategori laporan tidak valid. Pilihan yang valid adalah: Teknis, Akademis, Administratif, Lainnya.',
                    'status_tiket.required' => 'Status tiket wajib diisi.',
                    'status_tiket.in' => 'Status tiket tidak valid. Pilihan yang valid adalah: Belum Diproses, Sedang Diproses, Selesai.',
                    'tingkat_urgensi.required' => 'Tingkat urgensi wajib diisi.',
                    'tingkat_urgensi.in' => 'Tingkat urgensi tidak valid. Pilihan yang valid adalah: Rendah, Sedang, Tinggi.',
                ]);
    
                // Cari pengguna berdasarkan ID
                $tiket = Tiket::findOrFail($request->id);
    
                // Update data tiket berdasarkan data yang dikirim dari form
                $tiket->judul_tiket = $request->judul_tiket;
                $tiket->waktu_tiket = $request->waktu_tiket;
                $tiket->kategori_laporan = $request->kategori_laporan;
                $tiket->status_tiket = $request->status_tiket;
                $tiket->tingkat_urgensi = $request->tingkat_urgensi;
                $tiket->save();
    
                // Redirect ke halaman yang sesuai setelah update sukses
    
                return response()->json(['success' => 'tiket berhasil diupdate']);

            } catch (\Throwable $th) {
                return response()->json(['message' => $th->getMessage() ], 500);
            }
        }


        function destroy(Request $request)
    {
        try {
            // Hapus semua lampiran tiket terkait terlebih dahulu
            LampiranTiket::whereIn('id_tiket', $request->dataHapus)->delete();
            // Hapus semua riwayat tiket terkait
            RiwayatTiket::whereIn('id_tiket', $request->dataHapus)->delete();
            // Kemudian hapus tiket
            Tiket::destroy($request->dataHapus);

            // Redirect ke halaman yang sesuai setelah delete sukses
            return response()->json(['success' => 'data tiket berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }
}
